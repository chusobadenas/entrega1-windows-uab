﻿using System;
using System.Windows.Input;
using Entrega1.ViewModels.Base;

namespace Entrega1.ViewModels {

    public class VMMain : VMBase {
        #region Properties

        private bool isEyeOpen;

        public bool IsEyeOpen {
            get {
                return isEyeOpen;
            }

            set {
                isEyeOpen = value;
                RaisePropertyChanged("IsEyeOpen");
            }
        }

        #endregion

        public VMMain() {
            this.IsEyeOpen = true;
            InitializeCommands();
        }

        #region Commands
        
        private Lazy<DelegateCommand<string>> eyeActionCommand;

        public ICommand EyeActionCommand {
            get {
                return eyeActionCommand.Value;
            }
        }

        private void InitializeCommands() {
            eyeActionCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(EyeActionCommandExecute,
                                                  EyeActionCommandCanExecute));
        }

        public bool EyeActionCommandCanExecute(object param) {
            return true;
        }

        public void EyeActionCommandExecute(string param) {
            // Change action
            IsEyeOpen = !IsEyeOpen;
        }

        #endregion
    }
}
