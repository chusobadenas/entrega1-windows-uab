﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Entrega1.Converters {

    public class BoolToImgPathConverter : IValueConverter {

        object IValueConverter.Convert(object value, Type targetType, object parameter, string language) {
            string imgPath = "/Assets/icon-eye-close.png";

            if (value != null) {
                bool currentValue = false;
                bool.TryParse(value.ToString(), out currentValue);

                if (currentValue) {
                    imgPath = "/Assets/icon-eye-open.png";
                }
            }

            return imgPath;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotImplementedException();
        }
    }
}
