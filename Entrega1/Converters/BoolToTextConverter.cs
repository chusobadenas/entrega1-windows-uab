﻿using System;
using System.Globalization;
using Windows.UI.Xaml.Data;

namespace Entrega1.Converters {

    public class BoolToTextConverter : IValueConverter {

        object IValueConverter.Convert(object value, Type targetType, object parameter, string language) {
            string text = "Open";

            if(value != null) {
                bool currentValue = false;
                bool.TryParse(value.ToString(), out currentValue);

                if(currentValue) {
                    text = "Close";
                }
            }

            return text;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotImplementedException();
        }
    }
}
